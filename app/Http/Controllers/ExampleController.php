<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use App\User;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
      return view('example.index');
    }

    public function search(Request $request){
      
      try {
        $user = User::findOrFail($request->input('user_id'));
      } catch (ModelNotFoundException $exception) {
        Log::stack(['app'])->error('Erro ao carregar o objeto ' . User::class . ', Mensagem: ' . $exception->getMessage() . ' [ ' . $request->ip() . ']');
        return back()->withError('User not found by ID ' . $request->input('user_id'))->withInput();
      }
      
      return view('example.search', compact('user'));
    }

    public function escopo(){
      $user = User::ofName('Elian McCullough')->first();
      dd($user);
    }
}
