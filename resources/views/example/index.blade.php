@extends('layouts.app')

@section('content')
@if (session('error'))
    <div class="alert alert-danger">{{ session('error') }}</div>
@endif
	<form action="{{ route('example.search') }}" method="POST">
	    @csrf
	 
	    <div class="form-group">
	        <input name="user_id" type="text" id="user_id" placeholder="User ID" 
	            class="form-control" value="{{ old('user_id') }}">
	    </div>
	 
	    <input type="submit" class="btn btn-info" value="Search">
	</form>
@endsection